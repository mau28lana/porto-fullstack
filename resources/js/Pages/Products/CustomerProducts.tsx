import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, router, usePage } from "@inertiajs/react";
import { PageProps, Product } from "@/types";
import ProductList from "@/Pages/Products/components/ProductList";
import Paginator from "@/Components/Paginator";
import { useEffect, useState } from "react";

const Products: React.FC<PageProps> = ({ auth, products, flash }) => {
    const { selectedCategory }: any = usePage().props;
    const [showAlert, setShowAlert] = useState(true);
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);

    const handleCategorySelect = (category: string) => {
        setIsDropdownOpen(false);

        router.visit(`/products/${category}`);
    };

    useEffect(() => {
        if (!products) {
            router.get("/products");
        }
        const delay = setTimeout(() => {
            setShowAlert(false);
        }, 10000);
        return () => clearTimeout(delay);
    }, []);

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Products
                </h2>
            }
        >
            <Head title="Dashboard" />

            {showAlert && flash.message && (
                <div className="alert alert-success p-6">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="stroke-current shrink-0 h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                    </svg>
                    <span>Data berhasil dibuat!</span>
                </div>
            )}

            <div className="flex justify-between p-6">
                <div className="dropdown inline-block relative">
                    <label
                        tabIndex={0}
                        className="btn btn-secondary text-white flex items-center justify-between w-40"
                        onClick={() => setIsDropdownOpen(!isDropdownOpen)}
                    >
                        <span>{selectedCategory ?? "All"}</span>
                        <span>v</span>
                    </label>
                    {isDropdownOpen && (
                        <ul
                            tabIndex={0}
                            className="dropdown-content absolute left-0 mt-2 z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
                        >
                            <li>
                                <a onClick={() => handleCategorySelect("")}>
                                    All
                                </a>
                            </li>
                            <li>
                                <a
                                    onClick={() =>
                                        handleCategorySelect("Manis")
                                    }
                                >
                                    Manis
                                </a>
                            </li>
                            <li>
                                <a
                                    onClick={() =>
                                        handleCategorySelect("Gurih")
                                    }
                                >
                                    Gurih
                                </a>
                            </li>
                            <li>
                                <a
                                    onClick={() =>
                                        handleCategorySelect("Pedas")
                                    }
                                >
                                    Pedas
                                </a>
                            </li>
                        </ul>
                    )}
                </div>
            </div>

            <ProductList products={products} />

            <Paginator products={products} />
            <div className="mt-52">{""}</div>
        </AuthenticatedLayout>
    );
};

export default Products;
