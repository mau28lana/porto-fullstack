import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, router, usePage } from "@inertiajs/react";
import { PageProps } from "@/types";
import AdminProductList from "@/Pages/Products/components/AdminProductsList";
import Paginator from "@/Components/Paginator";
import AddProduct from "@/Pages/Products/components/AddProduct";
import { useEffect, useState } from "react";

const AdminProducts: React.FC<PageProps> = ({ auth, products, flash }) => {
    const { selectedCategory }: any = usePage().props;
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const [showAlert, setShowAlert] = useState(false);

    useEffect(() => {
        if (!products) {
            router.get("/products");
            if (flash.message !== null) {
                setShowAlert(true);
            }
        }
    }, [flash.message, products]);

    if (showAlert === true) {
        if (flash.message !== null) {
            setTimeout(() => {
                setShowAlert(false);
            }, 10000);
            console.log(flash.message);
        } else {
            setShowAlert(false);
        }
    }

    const handleCategorySelect = (category: string) => {
        setIsDropdownOpen(false);

        router.visit(`/products/${category}`);
    };

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Products
                </h2>
            }
        >
            <Head title="Dashboard" />

            <div className="flex justify-between p-6">
                <div className="dropdown inline-block relative">
                    <label
                        tabIndex={0}
                        className="btn btn-secondary text-white flex items-center justify-between w-40"
                        onClick={() => setIsDropdownOpen(!isDropdownOpen)}
                    >
                        <span>{selectedCategory ?? "All"}</span>
                        <span>v</span>
                    </label>
                    {isDropdownOpen && (
                        <ul
                            tabIndex={0}
                            className="dropdown-content absolute left-0 mt-2 z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
                        >
                            <li>
                                <a onClick={() => handleCategorySelect("")}>
                                    All
                                </a>
                            </li>
                            <li>
                                <a
                                    onClick={() =>
                                        handleCategorySelect("Manis")
                                    }
                                >
                                    Manis
                                </a>
                            </li>
                            <li>
                                <a
                                    onClick={() =>
                                        handleCategorySelect("Gurih")
                                    }
                                >
                                    Gurih
                                </a>
                            </li>
                            <li>
                                <a
                                    onClick={() =>
                                        handleCategorySelect("Pedas")
                                    }
                                >
                                    Pedas
                                </a>
                            </li>
                        </ul>
                    )}
                </div>
                <AddProduct setShowAlert={setShowAlert} />
            </div>

            {showAlert && (
                <div className="relative p-6">
                    <div className="alert alert-success">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="stroke-current shrink-0 h-6 w-6"
                            fill="none"
                            viewBox="0 0 24 24"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                            />
                        </svg>
                        <span>{flash.message}</span>
                    </div>
                </div>
            )}

            <AdminProductList products={products} flash={flash} />

            <Paginator products={products} />
        </AuthenticatedLayout>
    );
};

export default AdminProducts;
