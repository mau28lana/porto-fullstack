import React, { useEffect, useState } from "react";
import { Flash, Meta, Product } from "@/types";
import { router } from "@inertiajs/react";
import EditProduct from "../../../Components/EditProduct";

interface ProductListProps {
    products: { data: Product[]; meta: Meta };
    flash: Flash;
}

const ProductList: React.FC<ProductListProps> = ({ products, flash }) => {
    const [isLoading, setIsLoading] = useState(true);
    const initialItemCounts = products.data.map(() => 0);
    const [itemCounts, setItemCounts] = useState(initialItemCounts);

    const [showAlert, setShowAlert] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 1500);
    }, []);

    const addItem = (index: number) => {
        const updatedCounts = [...itemCounts];
        if (updatedCounts[index] < products.data[index].totalProduct) {
            updatedCounts[index] = updatedCounts[index] + 1;
            setItemCounts(updatedCounts);
        }
    };

    const decreaseItem = (index: number) => {
        const updatedCounts = [...itemCounts];
        updatedCounts[index] = updatedCounts[index] - 1;
        setItemCounts(updatedCounts);
        console.log();
    };

    const deleteProduct = (id: number) => {
        router.delete(`/products/delete/${id}`, {
            onSuccess: () => setShowAlert(true),
        });
        setTimeout(() => {
            setShowAlert(false);
        }, 10000);
    };

    const updateStock = async (id: number, total: number, i: number) => {
        const data = {
            totalProduct: total,
        };
        router.put(`/products/update/stock/${id}`, data, {
            onSuccess: () => {
                setShowAlert(true);
            },
        });

        const updatedCounts = [...itemCounts];
        updatedCounts[i] = 0;
        setItemCounts(updatedCounts);

        setTimeout(() => {
            setShowAlert(false);
        }, 10000);
    };

    return (
        <>
            {showAlert && (
                <div className="p-6">
                    <div className="alert alert-success">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="stroke-current shrink-0 h-6 w-6"
                            fill="none"
                            viewBox="0 0 24 24"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                            />
                        </svg>
                        <span>{flash.message}</span>
                    </div>
                </div>
            )}
            {isLoading ? (
                <div className="flex w-full h-52 justify-center items-center">
                    Loading...
                </div>
            ) : (
                <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3 p-6">
                    {products.data.map((data, i) => (
                        <div
                            key={i}
                            className="card bg-base-100 shadow-xl flex flex-col"
                        >
                            <figure className="h-1/2 overflow-hidden">
                                <img
                                    className="object-contain w-full h-full"
                                    src={`storage/${data.image_url}`}
                                    alt="Shoes"
                                />
                            </figure>
                            <div className="p-4 flex-grow">
                                <h2 className="text-lg font-semibold">
                                    {data.title}
                                </h2>
                                <p className="text-sm text-gray-500">
                                    {data.description}
                                </p>
                                <h3 className="font-semibold mt-4">
                                    {"Harga: Rp. "}
                                    {data.price}
                                    {",- "}
                                </h3>
                            </div>
                            <div className="flex w-full justify-between items-center px-4">
                                <div className="flex flex-col w-full">
                                    <div className="flex mb-2 justify-start items-start">
                                        Stock:{" "}
                                        <span className="font-semibold ml-2">
                                            {" "}
                                            {data.totalProduct}
                                        </span>
                                    </div>

                                    <div className="join">
                                        <button
                                            className="join-item btn btn-warning"
                                            onClick={() => decreaseItem(i)}
                                        >
                                            -
                                        </button>
                                        <input
                                            className="join-item btn w-14 [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none"
                                            type="number"
                                            value={
                                                itemCounts[i] != null
                                                    ? itemCounts[i]
                                                    : 0
                                            }
                                            onChange={(e) => {
                                                const newValue =
                                                    parseInt(e.target.value) ||
                                                    0;
                                                const updatedCounts = [
                                                    ...itemCounts,
                                                ];
                                                updatedCounts[i] = newValue;
                                                setItemCounts(updatedCounts);
                                            }}
                                        />
                                        <button
                                            className="join-item btn btn-warning"
                                            onClick={() => addItem(i)}
                                        >
                                            +
                                        </button>
                                    </div>
                                </div>
                                <div className="flex flex-col w-full justify-end items-end">
                                    <div className="badge badge-outline mb-2 ">
                                        {data.category}
                                    </div>
                                    <button
                                        className="btn btn-primary xs:w-20 text-white"
                                        onClick={async () => {
                                            await updateStock(
                                                data.id,
                                                data.totalProduct +
                                                    itemCounts[i],
                                                i
                                            );
                                        }}
                                    >
                                        Tambah Stock
                                    </button>
                                </div>
                            </div>
                            <div className="flex justify-between p-4">
                                <EditProduct product={data} />
                                <button
                                    className="btn btn-error text-white w-1/3"
                                    onClick={() => deleteProduct(data.id)}
                                >
                                    delete
                                </button>
                            </div>
                        </div>
                    ))}
                </div>
            )}
        </>
    );
};

export default ProductList;
