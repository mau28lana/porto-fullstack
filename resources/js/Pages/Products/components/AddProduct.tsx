import React, { useEffect, useState } from "react";
import { Link, router } from "@inertiajs/react";
import { Flash } from "@/types";

interface props {
    setShowAlert: (value: boolean) => void;
}

const AddProduct = ({ setShowAlert }: props) => {
    const [category, setCategory] = useState("");

    const [isDropdownModalOpen, setIsDropdownModalOpen] = useState(false);

    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [totalProduct, setTotalProduct] = useState(0);
    const [image_url, setImageUrl] = useState<File | undefined>();

    const categoryModalSelect = (category: string) => {
        setCategory(category);
        setIsDropdownModalOpen(false);
    };

    const toggleModalDropdown = () => {
        setIsDropdownModalOpen((prevState) => !prevState);
    };

    const handleSubmit = async () => {
        const data = {
            title,
            description,
            price,
            totalProduct,
            category,
            image_url,
        };
        router.post("/products", data, {
            onSuccess: () => setShowAlert(true),
        });
    };
    return (
        <>
            <button
                className="btn btn-primary text-white"
                onClick={() => window.addProduct.showModal()}
            >
                Tambah produk
            </button>
            <dialog id="addProduct" className="modal">
                <form
                    method="dialog"
                    className="modal-box h-4/6"
                    encType="multipart/form-data"
                    onSubmit={handleSubmit}
                >
                    <Link
                        href="/products"
                        className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                    >
                        ✕
                    </Link>
                    <h3 className="font-bold text-lg">Tambah Produk</h3>

                    <input
                        type="text"
                        placeholder="Judul"
                        onChange={(title) => setTitle(title.target.value)}
                        className="input input-bordered w-full mt-4"
                    />
                    {title == "" && (
                        <p className="text-red-600 mt-2">
                            Judul tidak boleh kosong!
                        </p>
                    )}
                    <input
                        type="text"
                        placeholder="Deskripsi"
                        onChange={(description) =>
                            setDescription(description.target.value)
                        }
                        className="input input-bordered w-full mt-4"
                    />
                    {description == "" && (
                        <p className="text-red-600 mt-2">
                            Deskripsi tidak boleh kosong!
                        </p>
                    )}
                    <input
                        type="number"
                        placeholder="Harga"
                        onChange={(price) =>
                            setPrice(price.target.valueAsNumber)
                        }
                        className="input input-bordered w-full mt-4"
                    />
                    {price == 0 && (
                        <p className="text-red-600 mt-2">
                            Harga tidak boleh kosong!
                        </p>
                    )}
                    <input
                        type="number"
                        placeholder="Stok"
                        onChange={(totalProduct) =>
                            setTotalProduct(totalProduct.target.valueAsNumber)
                        }
                        className="input input-bordered w-full mt-4"
                    />
                    {totalProduct == 0 && (
                        <p className="text-red-600 mt-2">
                            Stok tidak boleh kosong!
                        </p>
                    )}
                    <input
                        type="file"
                        className="file-input file-input-bordered file-input-secondary w-full mt-4"
                        onChange={(image_url) => {
                            const selectedImage =
                                image_url.target.files &&
                                image_url.target.files[0];
                            setImageUrl(selectedImage!);
                        }}
                    />
                    {image_url == null && (
                        <p className="text-red-600 mt-2">
                            Gambar tidak boleh kosong!
                        </p>
                    )}
                    <div className="dropdown inline-block mt-4 w-full">
                        <label
                            tabIndex={0}
                            className="btn btn-secondary text-white flex items-center justify-between w-full"
                            onClick={toggleModalDropdown}
                        >
                            <span>
                                {category != "" ? category : "Kategori"}
                            </span>
                            <span>v</span>
                        </label>
                        {isDropdownModalOpen && (
                            <ul
                                tabIndex={0}
                                className="dropdown-content left-0 mt-2 mb-8 z-[1] menu p-2 shadow bg-base-100 rounded-box w-full"
                            >
                                <li>
                                    <a
                                        onClick={() =>
                                            categoryModalSelect("Manis")
                                        }
                                    >
                                        Manis
                                    </a>
                                </li>
                                <li>
                                    <a
                                        onClick={() =>
                                            categoryModalSelect("Gurih")
                                        }
                                    >
                                        Gurih
                                    </a>
                                </li>
                                <li>
                                    <a
                                        onClick={() =>
                                            categoryModalSelect("Pedas")
                                        }
                                    >
                                        Pedas
                                    </a>
                                </li>
                            </ul>
                        )}
                    </div>
                    {category == "" && (
                        <p className="text-red-600 mt-2">
                            Pilih salah satu kategori
                        </p>
                    )}
                    <div className="flex w-full justify-end">
                        <button
                            type="submit"
                            className="btn btn-primary text-white mt-6"
                        >
                            Tambahkan produk
                        </button>
                    </div>
                </form>
            </dialog>
        </>
    );
};

export default AddProduct;
