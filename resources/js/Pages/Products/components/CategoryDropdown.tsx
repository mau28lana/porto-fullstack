// import { router } from "@inertiajs/react";
// import { useState } from "react";
// import AddProduct from "./AddProduct";
// import { Flash } from "@/types";

// interface CategoryDropdownProps {
//     selectedCategory: any;
//     flash: Flash;
// }

// export default function CategoryDropdown({
//     selectedCategory,
//     flash,
// }: CategoryDropdownProps) {
//     const [isDropdownOpen, setIsDropdownOpen] = useState(false);

//     const handleCategorySelect = (category: string) => {
//         setIsDropdownOpen(false);

//         router.visit(`/products/${category}`);
//     };

//     return (
//         <div className="flex flex-col justify-between p-6">
//             <div>
//             <div className="dropdown inline-block relative">
//                 <label
//                     tabIndex={0}
//                     className="btn btn-secondary text-white flex items-center justify-between w-40"
//                     onClick={() => setIsDropdownOpen(!isDropdownOpen)}
//                 >
//                     <span>{selectedCategory ?? "All"}</span>
//                     <span>v</span>
//                 </label>
//                 {isDropdownOpen && (
//                     <ul
//                         tabIndex={0}
//                         className="dropdown-content absolute left-0 mt-2 z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
//                     >
//                         <li>
//                             <a onClick={() => handleCategorySelect("")}>All</a>
//                         </li>
//                         <li>
//                             <a onClick={() => handleCategorySelect("Manis")}>
//                                 Manis
//                             </a>
//                         </li>
//                         <li>
//                             <a onClick={() => handleCategorySelect("Gurih")}>
//                                 Gurih
//                             </a>
//                         </li>
//                         <li>
//                             <a onClick={() => handleCategorySelect("Pedas")}>
//                                 Pedas
//                             </a>
//                         </li>
//                     </ul>
//                 )}
//             </div>
//             <AddProduct flash={flash} />
//             </div>
//             {showAlert && (
//                 <div className="relative">
//                     <div className="alert alert-success">
//                         <svg
//                             xmlns="http://www.w3.org/2000/svg"
//                             className="stroke-current shrink-0 h-6 w-6"
//                             fill="none"
//                             viewBox="0 0 24 24"
//                         >
//                             <path
//                                 strokeLinecap="round"
//                                 strokeLinejoin="round"
//                                 strokeWidth="2"
//                                 d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
//                             />
//                         </svg>
//                         <span>{flash.message}</span>
//                     </div>
//                 </div>
//             )}
//         </div>
//     );
// }
