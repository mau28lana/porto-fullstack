import React, { useEffect, useState } from "react";
import { Meta, Product } from "@/types";
import { router } from "@inertiajs/react";

interface ProductListProps {
    products: { data: Product[]; meta: Meta };
}

const ProductList: React.FC<ProductListProps> = ({ products }) => {
    const [isLoading, setIsLoading] = useState(true);
    const initialItemCounts = products.data.map(() => 0);
    const [itemCounts, setItemCounts] = useState(initialItemCounts);
    const [selectedProducts, setSelectedProducts] = useState<
        { product: Product; quantity: number }[]
    >([]);

    useEffect(() => {
        const delay = setTimeout(() => {
            setIsLoading(false);
        }, 1500);
        return () => clearTimeout(delay);
    }, []);

    const addItem = (index: number, product: Product) => {
        const updatedCounts = [...itemCounts];
        const existingItem = selectedProducts.find(
            (item) => item.product.id === product.id
        );

        if (updatedCounts[index] < products.data[index].totalProduct) {
            if (existingItem) {
                setSelectedProducts((prevItems) =>
                    prevItems.map((item) =>
                        item.product.id === product.id
                            ? { ...item, quantity: item.quantity + 1 }
                            : item
                    )
                );
            } else {
                setSelectedProducts((prevItems) => [
                    ...prevItems,
                    { product, quantity: 1 },
                ]);
            }
            updatedCounts[index] = updatedCounts[index] + 1;
            setItemCounts(updatedCounts);
        }
    };

    const decreaseItem = (index: number, product: Product) => {
        const updatedCounts = [...itemCounts];
        const existingItem = selectedProducts.find(
            (item) => item.product.id === product.id
        );

        if (existingItem) {
            if (existingItem.quantity > 0) {
                setSelectedProducts((prevItems) =>
                    prevItems.map((item) =>
                        item.product.id === product.id
                            ? { ...item, quantity: item.quantity - 1 }
                            : item
                    )
                );
                if (updatedCounts[index] !== 0) {
                    updatedCounts[index] = updatedCounts[index] - 1;
                    setItemCounts(updatedCounts);
                }
            } else {
                setSelectedProducts((prevItems) =>
                    prevItems.filter((item) => item.product.id !== product.id)
                );
                updatedCounts[index] = 0;
                setItemCounts(updatedCounts);
            }
            console.log(selectedProducts);
        } else {
            setSelectedProducts((prevItems) => [
                ...prevItems,
                { product, quantity: 0 },
            ]);
        }
    };

    const calculateTotalPrice = () => {
        return selectedProducts.reduce((total, item) => {
            const productPrice = item.product.price;
            const quantity = item.quantity;
            return total + productPrice * quantity;
        }, 0);
    };

    const submitChart = async () => {
        const selectedProductsData = selectedProducts.map((item) => ({
            product_id: item.product.id,
            quantity: item.quantity,
        }));
        const data = {
            selectedProductsData,
        };
        console.log(data);

        try {
            router.post("/charts/add", data);
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <>
            {isLoading ? (
                <div className="flex w-full h-52 justify-center items-center">
                    Loading...
                </div>
            ) : (
                <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3 p-6 relative">
                    {products.data.map((data, i) => (
                        <div
                            key={i}
                            className="card bg-base-100 shadow-xl flex flex-col"
                        >
                            <figure className="h-1/2 overflow-hidden">
                                <img
                                    className="object-contain w-full h-full"
                                    src={`storage/${data.image_url}`}
                                    alt="Shoes"
                                />
                            </figure>
                            <div className="p-4 flex-grow">
                                <h2 className="text-lg font-semibold">
                                    {data.title}
                                </h2>
                                <p className="text-sm text-gray-500">
                                    {data.description}
                                </p>
                                <h3 className="font-semibold mt-4">
                                    {"Harga: Rp. "}
                                    {data.price}
                                    {",- "}
                                </h3>
                            </div>
                            <div className="flex w-full justify-between items-center px-4">
                                <div className="flex flex-col w-full pb-7">
                                    <div className="flex mb-2 justify-start items-start">
                                        Stock:{" "}
                                        <span className="font-semibold ml-2">
                                            {" "}
                                            {data.totalProduct}
                                        </span>
                                    </div>

                                    <div className="join">
                                        <div className="join">
                                            <button
                                                className="join-item btn btn-warning"
                                                onClick={() =>
                                                    decreaseItem(i, data)
                                                }
                                            >
                                                -
                                            </button>
                                            <input
                                                className="join-item btn w-14 [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none"
                                                type="number"
                                                value={
                                                    itemCounts[i] != null
                                                        ? itemCounts[i]
                                                        : 0
                                                }
                                                onChange={(e) => {
                                                    const newValue =
                                                        parseInt(
                                                            e.target.value
                                                        ) || 0;
                                                    const updatedCounts = [
                                                        ...itemCounts,
                                                    ];
                                                    updatedCounts[i] = newValue;
                                                    setItemCounts(
                                                        updatedCounts
                                                    );
                                                }}
                                            />
                                            <button
                                                className="join-item btn btn-warning"
                                                onClick={() => addItem(i, data)}
                                            >
                                                +
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex flex-col w-full justify-end items-end pb-7">
                                    <div className="badge badge-outline mb-2 ">
                                        {data.category}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                    {selectedProducts.length > 0 && (
                        <div className="fixed bottom-0 w-3/4 bg-white border-t shadow z-10">
                            <h2 className="mb-2 text-lg text-white font-bold bg-primary rounded-t-lg px-4 py-2">
                                Selected Products
                            </h2>
                            <ul className="grid grid-cols-1 gap-4 md:grid-cols-2 h-20 md:gap-4 overflow-x-auto px-4">
                                {selectedProducts.map((item, index) => (
                                    <li
                                        key={index}
                                        className="border p-2 rounded"
                                    >
                                        <div>
                                            <strong>
                                                {item.product.title}
                                            </strong>
                                        </div>
                                        <div>Quantity: {item.quantity}</div>
                                    </li>
                                ))}
                            </ul>
                            <div className="flex w-full justify-between px-4 pb-2">
                                <div className="mt-4 text-md font-semibold">
                                    Total Price: Rp. {calculateTotalPrice()}
                                </div>
                                <button
                                    className="btn btn-primary text-white"
                                    onClick={submitChart}
                                >
                                    Submit Chart
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            )}
        </>
    );
};

export default ProductList;
