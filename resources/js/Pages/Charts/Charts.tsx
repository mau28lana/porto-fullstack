import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Chart, PageProps } from "@/types";
import { Head, Link } from "@inertiajs/react";

interface ChartProps {
    chart: Chart;
}

const Charts: React.FC<PageProps> = ({ auth, charts }) => {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Charts
                </h2>
            }
        >
            <Head title="Chats" />
        </AuthenticatedLayout>
    );
};

export default Charts;
