import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head } from "@inertiajs/react";
import { PageProps } from "@/types";
import { ExclamationCircleIcon } from "@heroicons/react/24/outline";

export default function Dashboard({ auth }: PageProps) {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Dashboard
                </h2>
            }
        >
            <Head title="Dashboard" />
            <div className="p-6">
                <div className="alert alert-success">
                    <ExclamationCircleIcon className="h-6 w-6" />
                    <span className="font-bold">You are now logged in!</span>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
