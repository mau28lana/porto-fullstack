import { Fragment, useEffect, useRef, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { Product } from "@/types";
import { router } from "@inertiajs/react";

interface EditProductProps {
    product: Product;
}
export default function EditProduct({ product }: EditProductProps) {
    const [open, setOpen] = useState(false);
    const [productData, setProductData] = useState<Product>();
    const [category, setCategory] = useState("");
    const [isDropdownModalOpen, setIsDropdownModalOpen] = useState(false);

    const [productId, setProductId] = useState(0);
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [totalProduct, setTotalProduct] = useState(0);

    const categoryModalSelect = (category: string) => {
        setCategory(category);
        setIsDropdownModalOpen(false);
    };

    const cancelButtonRef = useRef(null);

    const toggleModalDropdown = () => {
        setIsDropdownModalOpen((prevState) => !prevState);
    };

    useEffect(() => {
        setProductData(product);
        setProductId(product.id);
        setTitle(product.title);
        setDescription(product.description);
        setPrice(product.price);
        setTotalProduct(product.totalProduct);
        setCategory(product.category);
    }, []);

    const handleSubmit = () => {
        const data = {
            title,
            description,
            price,
            totalProduct,
            category,
        };

        try {
            router.visit(`/products/update/${product.id}`, {
                data: data,
                method: "put",
            });
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <button
                type="button"
                className="btn btn-primary text-white w-1/3"
                onClick={() => setOpen(true)}
            >
                Edit
            </button>
            <Transition.Root show={open} as={Fragment}>
                <Dialog
                    as="div"
                    className="relative z-10"
                    initialFocus={cancelButtonRef}
                    onClose={setOpen}
                >
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                    </Transition.Child>

                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                                enterTo="opacity-100 translate-y-0 sm:scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                                leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            >
                                <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
                                    <form
                                        method="dialog"
                                        className="p-6"
                                        onSubmit={handleSubmit}
                                    >
                                        <h3 className="font-bold text-lg">
                                            Edit Product
                                        </h3>

                                        <input
                                            type="text"
                                            placeholder={productData?.title}
                                            onChange={(title) =>
                                                setTitle(title.target.value)
                                            }
                                            className="input input-bordered w-full mt-4"
                                        />
                                        <input
                                            type="text"
                                            placeholder={
                                                productData?.description
                                            }
                                            onChange={(description) =>
                                                setDescription(
                                                    description.target.value
                                                )
                                            }
                                            className="input input-bordered w-full mt-4"
                                        />
                                        <input
                                            type="number"
                                            placeholder={productData?.price.toString()}
                                            onChange={(price) =>
                                                setPrice(
                                                    price.target.valueAsNumber
                                                )
                                            }
                                            className="input input-bordered w-full mt-4"
                                        />
                                        <input
                                            type="number"
                                            placeholder={productData?.totalProduct.toString()}
                                            onChange={(totalProduct) =>
                                                setTotalProduct(
                                                    totalProduct.target
                                                        .valueAsNumber
                                                )
                                            }
                                            className="input input-bordered w-full mt-4"
                                        />
                                        <div className="dropdown inline-block mt-4 w-full">
                                            <label
                                                tabIndex={0}
                                                className="btn btn-secondary text-white flex items-center justify-between w-full"
                                                onClick={toggleModalDropdown}
                                            >
                                                <span>
                                                    {productData?.category}
                                                </span>
                                                <span>v</span>
                                            </label>
                                            {isDropdownModalOpen && (
                                                <ul
                                                    tabIndex={0}
                                                    className="dropdown-content left-0 mt-2 mb-8 z-[1] menu p-2 shadow bg-base-100 rounded-box w-full"
                                                >
                                                    <li>
                                                        <a
                                                            onClick={() =>
                                                                categoryModalSelect(
                                                                    "Manis"
                                                                )
                                                            }
                                                        >
                                                            Manis
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a
                                                            onClick={() =>
                                                                categoryModalSelect(
                                                                    "Gurih"
                                                                )
                                                            }
                                                        >
                                                            Gurih
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a
                                                            onClick={() =>
                                                                categoryModalSelect(
                                                                    "Pedas"
                                                                )
                                                            }
                                                        >
                                                            Pedas
                                                        </a>
                                                    </li>
                                                </ul>
                                            )}
                                        </div>
                                        <div className="bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
                                            <button
                                                type="submit"
                                                className="btn btn-primary text-white"
                                                onClick={() => setOpen(false)}
                                            >
                                                Submit
                                            </button>
                                            <button
                                                type="button"
                                                className="btn btn-outline mr-4"
                                                onClick={() => setOpen(false)}
                                                ref={cancelButtonRef}
                                            >
                                                Cancel
                                            </button>
                                        </div>
                                    </form>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition.Root>
        </>
    );
}
