import { Meta, Product } from "@/types";
import { Link } from "@inertiajs/react";

interface ProductListProps {
    products: { data: Product[]; meta: Meta };
}

const Paginator: React.FC<ProductListProps> = ({ products }) => {
    const prev = products.meta.links[0].url;
    const next = products.meta.links[products.meta.links.length - 1].url;
    const goFirstPage = products.meta.links[1].url;
    const goLastPage = products.meta.links[products.meta.links.length - 2].url;
    const current = products.meta.current_page;
    const last_page = products.meta.last_page;

    return (
        <div className="join flex px-6 py-4 w-full justify-end">
            {current! > 2 && (
                <Link href={prev} className="join-item btn btn-outline">
                    «
                </Link>
            )}
            {current != 1 && (
                <Link href={goFirstPage} className="join-item btn btn-outline">
                    1
                </Link>
            )}
            <Link href="" className="join-item btn btn-disabled">
                <div className="text-black">{current}</div>
            </Link>
            {current != last_page && (
                <Link href={goLastPage} className="join-item btn btn-outline">
                    {last_page}
                </Link>
            )}
            {next && (
                <Link href={next} className="join-item btn btn-outline">
                    »
                </Link>
            )}
        </div>
    );
};

export default Paginator;
