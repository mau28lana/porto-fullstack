export interface User {
    id: number;
    name: string;
    email: string;
    email_verified_at: string;
}

export interface Product {
    id: number;
    title: string;
    description: string;
    price: number;
    totalProduct: number;
    category: string;
    image_url: string;
    pivot: {
        quantity: number;
    };
}

export interface Links {
    active: boolean;
    label: string;
    url: string;
}

export interface Meta {
    current_page: number;
    from: number;
    last_page: number;
    links: Links[];
    path: string;
    per_page: number;
    to: number;
    total: number;
}

export interface Flash {
    message: string;
}

export interface Chart {
    products: Product[];
}

export type PageProps<
    T extends Record<string, unknown> = Record<string, unknown>
> = T & {
    auth: {
        user: User;
    };

    products: { data: Product[]; meta: Meta };

    flash: Flash;

    charts: Chart;
};
