import { AxiosInstance } from "axios";
import ziggyRoute, { Config as ZiggyConfig } from "ziggy-js";

declare global {
    interface Window {
        axios: AxiosInstance;
        addProduct: {
            showModal: () => void;
        };
        editProduct: {
            showModal: () => void;
        };
    }

    var route: typeof ziggyRoute;
    var Ziggy: ZiggyConfig;
}
