import { useState, PropsWithChildren, ReactNode } from "react";
import ApplicationLogo from "@/Components/ApplicationLogo";
import Dropdown from "@/Components/Dropdown";
import NavLink from "@/Components/NavLink";
import { Link } from "@inertiajs/react";
import { User } from "@/types";

export default function Authenticated({
    user,
    header,
    children,
}: PropsWithChildren<{ user: User; header?: ReactNode }>) {
    return (
        <div className="flex min-h-screen bg-gray-100">
            <div className="w-1/5 bg-white border-r border-gray-200 flex flex-col">
                <div className="flex p-4 justify-between">
                    <Link href="/">
                        <ApplicationLogo className="block h-9 w-auto fill-current text-gray-800" />
                    </Link>
                    <div className="hidden sm:flex">
                        <div className="relative">
                            <Dropdown>
                                <Dropdown.Trigger>
                                    <span className="inline-flex rounded-md">
                                        <button
                                            type="button"
                                            className="inline-flex items-center text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition ease-in-out duration-150"
                                        >
                                            <div className="flex flex-col items-start">
                                                <span>{user.name}</span>
                                                <span className=" overflow-hidden text-xs text-gray-400 max-w-full truncate">
                                                    {user.email}
                                                </span>
                                            </div>

                                            <svg
                                                className="ml-2 -mr-0.5 h-4 w-4"
                                                xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 20 20"
                                                fill="currentColor"
                                            >
                                                <path
                                                    fillRule="evenodd"
                                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                    clipRule="evenodd"
                                                />
                                            </svg>
                                        </button>
                                    </span>
                                </Dropdown.Trigger>

                                <Dropdown.Content>
                                    <Dropdown.Link href={route("profile.edit")}>
                                        Profile
                                    </Dropdown.Link>
                                    <Dropdown.Link
                                        href={route("logout")}
                                        method="post"
                                        as="button"
                                    >
                                        Log Out
                                    </Dropdown.Link>
                                </Dropdown.Content>
                            </Dropdown>
                        </div>
                    </div>
                </div>
                <div className="p-2 m-4 bg-gray-100 rounded-lg">
                    <div className="flex flex-col space-y-2 p-2">
                        <NavLink
                            href={route("dashboard")}
                            active={route().current("dashboard")}
                        >
                            Dashboard
                        </NavLink>
                    </div>
                    <div className="flex flex-col space-y-2 p-2">
                        <NavLink
                            href={route("products")}
                            active={route().current("products")}
                        >
                            Products
                        </NavLink>
                    </div>
                    <div className="flex flex-col space-y-2 p-2">
                        <NavLink
                            href={route("charts")}
                            active={route().current("charts")}
                        >
                            Charts
                        </NavLink>
                    </div>
                    <div className="flex flex-col space-y-2 p-2">
                        <NavLink
                            href={route("profile.edit")}
                            active={route().current("profile.edit")}
                        >
                            Profile
                        </NavLink>
                    </div>
                    {/* <div className="p-4">
                        <button
                            type="button"
                            className="flex items-start w-full text-gray-600 hover:text-gray-800 bg-gray-200 rounded-md p-2"
                            onClick={toggleCategoryDropdown}
                        >
                            <div className="flex justify-between w-full">
                                Category
                                <svg
                                    className="ml-2 -mr-0.5 h-4 w-4"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                            </div>
                        </button>
                        {showCategoryDropdown && (
                            <div className="mt-2 pl-4">
                                <NavLink
                                    href={route("profile.edit")}
                                    active={route().current("profile.edit")}
                                >
                                    Category Page 1
                                </NavLink>
                                <NavLink
                                    href={route("dashboard")}
                                    active={route().current("profile.edit")}
                                >
                                    Category Page 2
                                </NavLink>
                            </div>
                        )}
                    </div> */}
                </div>
            </div>

            <div className="flex flex-col w-4/5 bg-gray-100">
                <main className="">
                    {header && (
                        <header className="bg-white shadow">
                            <div className="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                                {header}
                            </div>
                        </header>
                    )}
                    {children}
                </main>
            </div>
        </div>
    );
}
