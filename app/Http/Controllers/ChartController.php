<?php

namespace App\Http\Controllers;

use App\Models\Chart;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ChartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $chart = Chart::with('products')->where('user_id', auth()->id())->firstOrFail();

        return Inertia::render('Charts/Charts', [
            'charts' => $chart,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $chart = Chart::updateOrCreate(
            ['user_id' => $user->id],
            []
        );

        foreach ($request->selectedProductsData as $productData) {
            $product = Products::find($productData['id']);
            if ($product) {
                $chart->products()->syncWithoutDetaching([
                    $product->id => ['quantity' => $productData['quantity']],
                ]);
            }
        }


        return Redirect::route('charts.add')->with(['message' => 'Products added to chart successfully']);
    }


    /**
     * Display the specified resource.
     */
    public function show(Chart $chart, $id)
    {
        $chart = Chart::with('products')->findOrFail($id);

        return Inertia::render('Chart/Show', ['chart' => $chart]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Chart $chart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Chart $chart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Chart $chart)
    {
        //
    }
}
