<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use App\Http\Resources\ProductsCollection;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Products::all();
        return Inertia::render('Dashboard', [
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Posts/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $products = new Products();

        $products->title = $request->title;
        $products->description = $request->description;
        $products->price = $request->price;
        $products->totalProduct = $request->totalProduct;
        $products->image_url = Storage::put('products-image', $request->file('image_url'));
        $products->category = $request->category;

        $products->save();

        return Redirect::route('products')->with('message', 'Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(Products $products, Request $request, $category = null)
    {
        $query = $category ? Products::where('category', $category) : Products::query();
        $products = new ProductsCollection($query->paginate(6));

        if ($request->user()->role !== 'admin') {

            return Inertia::render('Products/CustomerProducts', [
                'products' => $products,
                'selectedCategory' => $category,
            ]);
        } else {
            return Inertia::render('Products/AdminProducts', [
                'products' => $products,
                'selectedCategory' => $category,
            ]);
        }
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Products $products, Request $request)
    {

        $products = $products->find($request->id);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Products $products)
    {

        $products->update([
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price,
            'totalProduct' => $request->totalProduct,
            'category' => $request->category,
        ]);

        return Redirect::route('products')->with('message', 'Data berhasil diperbaharui');
    }

    public function updateStock(Request $request, Products $products)
    {
        $products->update([
            'totalProduct' => $request->totalProduct,
        ]);

        return Redirect::route('products')->with('message', 'Stock berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Products $products, $id)
    {
        $products = $products->find($id);

        $products->delete();

        return Redirect::route('products')->with('message', 'data berhasil dihapus');
    }
}
