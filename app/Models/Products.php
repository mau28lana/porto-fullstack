<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', // Add 'title' to the fillable attributes
        'description',
        'price',
        'totalProduct',
        'category',
        'image_url'
    ];
}
