<?php

use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ChartController;
use App\Models\Products;
use App\Http\Controllers\ProfileController;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});


Route::middleware(['auth', 'verified'])->group(function () {
    //product
    Route::get('/products', [ProductsController::class, 'show'])->middleware(['auth', 'verified'])->name('products');
    Route::get('/products/{category}', [ProductsController::class, 'show'])->middleware(['auth', 'verified'])->where('category', 'Manis|Gurih|Pedas')->name('products.category');;
    Route::get('/products/{category?}', function () {
        abort(404);
    });

    //charts
    Route::get('/charts', [ChartController::class, 'index'])->name('charts');
    Route::post('/charts/add', [ChartController::class, 'store'])->name('charts.add');
});


Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware('role:admin')->group(function () {
    Route::post('/products', [ProductsController::class, 'store'])->middleware(['auth', 'verified'])->name('create.products');
    Route::get('products/{productId?}', [ProductsController::class, 'edit'])->middleware(['auth', 'verified'])->name('edit.products');;
    Route::put('products/update/{products}', [ProductsController::class, 'update'])->middleware(['auth', 'verified'])->name('update.products');;
    Route::put('products/update/stock/{products}', [ProductsController::class, 'updateStock'])->middleware(['auth', 'verified'])->name('update.products.stock');;
    Route::delete('products/delete/{id}', [ProductsController::class, 'destroy'])->middleware(['auth', 'verified'])->name('delete.products');;
});



require __DIR__ . '/auth.php';
