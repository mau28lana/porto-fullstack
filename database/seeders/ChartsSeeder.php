<?php

namespace Database\Seeders;

use App\Models\Chart;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChartsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $userIds = [1, 2, 3];
        $productIds = [1, 2, 3];

        foreach ($userIds as $userId) {
            $chart = Chart::factory()->create([
                'user_id' => $userId,
            ]);

            foreach ($productIds as $productId) {
                $quantity = rand(1, 5);
                $chart->products()->attach($productId, ['quantity' => $quantity]);
            }
        }
    }
}
